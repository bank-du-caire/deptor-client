package com.company.deptorclient.web.screens.deptor_clients;

import com.haulmont.cuba.gui.screen.*;
import com.company.deptorclient.entity.DEPTOR_CLIENTS;

@UiController("debitorclient_DEPTOR_CLIENTS.browse")
@UiDescriptor("deptor_clients-browse.xml")
@LookupComponent("dEPTOR_CLIENTsesTable")
@LoadDataBeforeShow
public class DEPTOR_CLIENTSBrowse extends StandardLookup<DEPTOR_CLIENTS> {
}