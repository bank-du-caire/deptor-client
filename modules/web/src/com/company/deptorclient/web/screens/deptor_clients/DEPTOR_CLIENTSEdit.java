package com.company.deptorclient.web.screens.deptor_clients;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.DateField;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.deptorclient.entity.DEPTOR_CLIENTS;

import javax.inject.Inject;
import java.util.Date;

@UiController("debitorclient_DEPTOR_CLIENTS.edit")
@UiDescriptor("deptor_clients-edit.xml")
@EditedEntityContainer("dEPTOR_CLIENTSDc")
@LoadDataBeforeShow
public class DEPTOR_CLIENTSEdit extends StandardEditor<DEPTOR_CLIENTS> {
    @Inject
    private InstanceContainer<DEPTOR_CLIENTS> dEPTOR_CLIENTSDc;

    boolean editSatate = false;
    @Inject
    private Button ok;
    @Inject
    private Button confirmEdit;
    @Inject
    private Metadata metadata;
    @Inject
    private TextField<Integer> clientIdField;
    @Inject
    private TextField<String> accountNumField;
    @Inject
    private TextField<String> accountTypeField;
    @Inject
    private TextField<String> currencyTypeField;
    @Inject
    private DateField<Date> decisionDateField;
    @Inject
    private DateField<Date> endDateLimitField;
    @Inject
    private TextField<Integer> repaymentPeriodField;
    @Inject
    private DataManager dataManager;
    @Inject
    private Notifications notifications;
    @Inject
    private Screens screens;

    Integer clientId = null;
    Integer repaymentPeriod = null;
    Date decisionDate = null;
    Date endDateLimit = null;

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (dEPTOR_CLIENTSDc.getItem().getCreateTs() != null) {
            editSatate = true;
            ok.setVisible(false);
            confirmEdit.setVisible(true);
            clientId = clientIdField.getValue();
            repaymentPeriod = repaymentPeriodField.getValue();
            decisionDate = decisionDateField.getValue();
            endDateLimit = endDateLimitField.getValue();
        }
    }

    @Subscribe("confirmEdit")
    public void onConfirmEditClick(Button.ClickEvent event) {
        if (!clientId.equals(clientIdField.getValue()) || !repaymentPeriod.equals(repaymentPeriodField.getValue()) ||
                !decisionDate.equals(decisionDateField.getValue()) || !endDateLimit.equals(endDateLimitField.getValue())) {
            DEPTOR_CLIENTS deptor_clients = metadata.create(DEPTOR_CLIENTS.class);
            deptor_clients.setClientId(clientIdField.getValue());
            deptor_clients.setAccountNum(accountNumField.getValue());
            deptor_clients.setAccountType(accountTypeField.getValue());
            deptor_clients.setCurrencyType(currencyTypeField.getValue());
            deptor_clients.setDecisionDate(decisionDateField.getValue());
            deptor_clients.setEndDateLimit(endDateLimitField.getValue());
            deptor_clients.setRepaymentPeriod(repaymentPeriodField.getValue());
            dataManager.commit(deptor_clients);
            dataManager.remove(dEPTOR_CLIENTSDc.getItem());
            notifications.create().withCaption("تم التعديل بنجاح ..!").show();
        }
        screens.removeAll();
        screens.create(DEPTOR_CLIENTSBrowse.class, OpenMode.THIS_TAB).show();
    }


}