package com.company.deptorclient.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "DEPTOR_CLIENTS")
@Entity(name = "debitorclient_DEPTOR_CLIENTS")
public class DEPTOR_CLIENTS extends StandardEntity {
    private static final long serialVersionUID = 7123520648715254552L;

    @Column(name = "CLIENTID")
    private Integer clientId;

    @Column(name = "ACCOUNTNUM")
    private String accountNum;

    @Column(name = "ACCOUNTTYPE")
    private String accountType;

    @Column(name = "REPAYMENTPERIOD")
    private Integer repaymentPeriod;

    @Column(name = "DECISIONDATE")
    @Temporal(TemporalType.DATE)
    private Date decisionDate;

    @Column(name = "ENDDATELIMIT")
    private Date endDateLimit;

    @Column(name = "currency_type1")
    private String currencyType;


    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Integer getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public void setRepaymentPeriod(Integer repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }

    public Date getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }

    public Date getEndDateLimit() {
        return endDateLimit;
    }

    public void setEndDateLimit(Date endDateLimit) {
        this.endDateLimit = endDateLimit;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }


}