create table DEPTOR_CLIENTS (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CLIENTID number(10),
    ACCOUNTNUM varchar2(255 char),
    ACCOUNTTYPE varchar2(255 char),
    REPAYMENTPERIOD number(10),
    DECISIONDATE timestamp,
    ENDDATELIMIT timestamp,
    CREATEDDATE timestamp,
    CREATEDLOG varchar2(255 char),
    UPDATEDLOG varchar2(255 char),
    ENDDATE timestamp,
    ENDLOG varchar2(255 char),
    currency_type1 varchar2(255 char),
    --
    primary key (ID)
)^